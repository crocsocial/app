package com.gitlab.crocsocial.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SecurityConfig {

    /**
     *
     */
    private static final String LOGIN = "/#/login";

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
            .formLogin(
                t -> t.loginPage(LOGIN).defaultSuccessUrl("/#/home")
            )
            .logout(t -> t.logoutSuccessUrl(LOGIN))
            .authorizeHttpRequests(t -> t
                .requestMatchers("/api/**").hasRole("API")
                .requestMatchers("/ui/**").authenticated()
                .requestMatchers("/error").denyAll()
                .requestMatchers("/**").anonymous());
        return http.build();
    }
}
