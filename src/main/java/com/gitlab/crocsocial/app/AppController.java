package com.gitlab.crocsocial.app;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class AppController {
    @Autowired HttpServletRequest req;
    @Autowired HttpServletResponse res;

    @GetMapping("/current")
    Principal currentUser(Principal principal) {
        log.info("User: ", principal);
        return principal;
    }
}
