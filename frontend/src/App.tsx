import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Route, Routes } from 'react-router-dom';
import Auth from './components/Auth';

function App() {
  return (
    <>
      <Routes>
        <Route path='login' element={<div>login</div>}/>
        <Route path='home' element={<Auth element={<div>home</div>}/>}/>
        <Route path='' element={<Auth element={<div>home</div>}/>}/>
        <Route path='/*' element={<div>login</div>}/>
      </Routes>
    </>
  );
}

export default App;
