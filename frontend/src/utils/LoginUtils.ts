export default class LoginUtils {
    static async isLoggedIn(): Promise<boolean> {
        return await fetch('/current')
            .then(res => res.json())
            .then(json => true)
            .catch(e => {
                console.error("You are not logged in. Showing login page.");
                window.location.href = '/#/login';
                return false;
            });
    }
}