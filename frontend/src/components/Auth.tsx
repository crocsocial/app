import React, { Component, ReactNode } from "react";
import LoginUtils from "../utils/LoginUtils";

interface IAuth {
    element: React.JSX.Element;
}

interface AuthState {
    authenticated: boolean;
}

export default class Auth extends Component<IAuth> {
    state: Readonly<AuthState> = {
        authenticated: false
    };

    async componentDidMount(): Promise<void> {
        this.setState({authenticated: await LoginUtils.isLoggedIn()});
    }

    render(): ReactNode {
        return (
            <>
                {this.state.authenticated ? this.props.element : <></>}
            </>
        );
    }
}